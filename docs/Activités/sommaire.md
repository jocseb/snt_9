# Accueil des activités

!!! info "Consignes"
    Chacune des activités doit donner lieu à un travail:

    - soit un exposé (si mentionné);
    - soit un compte-rendu, rapide mais soigné, récapitulant les réponses aux questions posées dans l'activité;
    - ce travail doit être rendu au format `PDF`;
    - ce travail doit être rendu sur ...
