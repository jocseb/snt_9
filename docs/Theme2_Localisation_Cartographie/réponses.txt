Localisation et cartographie

Cours

### 1.1.1 Questions

!!! example "Exercice"
	=== "Conversion"
	**Convertir les coordonnées de Bordeaux au format DD.**

Coordonnées de Bordeaux :
en DMS :  
Latitude : 44°50’16” N  
Longitude : 0°34’46” O   

en DD :  
Latitude : 44 + 50/60 + 16/3600
= + 44.837778 °  
Le signe + marque le fait que cette latitude est dans l’hémisphère Nord.

Longitude : 0 + 34/60 + 46/3600
= - 0.579444 °  
Le signe − marque le fait que cette longitude est à l'ouest du méridien de Greenwich.

######################################################

"QCM : pour tester ses connaissances"

######################################################
### 2.2. Questions

!!! example "Exercice"
	=== "Fonctionnement de la géolocalisation"
		1. Pour indiquer sa position à l’utilisateur, un smartphone envoie-t-il une demande au réseau, à un central, à un satellite ou plusieurs ? (attention il y a un piège)

		2. Que reçoit-il d’un satellite ?

		3. Comment calcule-t-il sa distance avec un satellite ? 

		4. Quel matériel embarqué sur le satellite assure la précision ? 

		5. Où sont situés, géométriquement, tout les points à la même distance d’un satellite ? 

		6. Quelle est l’intersection de deux sphères ? De trois sphères ?

		7. Combien de satellites au minimum le smartphone doit-il capter ?

		8. Quel est le rôle du dernier satellite nécessaire ?

<!-- === "Réponses"
		1. Un récepteur GPS (ex. smartphone) **n'envoie rien**. Il ne fait que recevoir des signaux.
		2. Il reçoit d'un satellite un message contenant la position du satellite et l'heure d'envoi du message. 
		3. En mesurant le temps mis par le message pour parvenir jusqu'à lui, le récepteur GPS calcule la distance qui le sépare du satellite (grâce à la formule $d = V \times t$).
		4. Une horloge atomique assure la précision de l'heure à bord du satellite.
		5. Tous les points à la même distance d'un satellite sont situés une sphère.
		6. L'intersection de deux sphères est un cercle. L'intersection de trois sphères est 2 points.
		7. Le smartphone doit capter au minimum 4 satellites.
		8. Un 4ième satellite est nécessaire pour corriger l’erreur d’horloge du récepteur GPS, qui n’est pas aussi précis que les horloges atomiques embarquées dans les satellites.  -->

######################################################

!!! example "Exercice de calcul de distance"
	=== "Énoncé"
		Supposons que le signal d'un satellite S ait mis 0,071 seconde à me parvenir.  
		À quelle distance du satellite S suis-je ?  
		*On considèrera que le signal envoyé par le satellite a une vitesse de 300 000 km/s*

<!-- === "Correction"
		Formule : $vitesse [km/s] = \dfrac{distance [km]}{temps [s]}$ donc $distance = vitesse \times temps$  
		Application numérique : $300000 \times 0,071 = 21300$  
		Le satellite est à 21 300 km au-dessus de moi. -->

######################################################

!!! example "Exercice d'analyse critique d'une vidéo"
<!-- E: 53.945871  N: 27.840492 --> IRAN ???

################################################################################################################################
Act1
Exo1

| numéro de la ville | Latitude | Longitude | Ville |
        |:---:|---|---|---|
        | 1 | 51° N | 0° O| Londres |			GREENWICH
        | 2 | 49° N | 2° E| Paris |
        | 3 | 41° N | 116° O| New-York |
        | 4 | 0° N | 79° O| Quito |			EQUATEUR
        | 5 | 34° S | 18° E| Le Cap |
        | 6 | 34° S | 71° O| Santiago|
        | 7 | 40° N | 74° E | Pékin |

Exo2
    === "Énoncé"
        En utilisant le site ```coordonnees-gps.fr```, répondre aux 2 questions suivantes :  
        1. Que trouve-t-on aux coordonnées : 45.193867 , -0.747963 ? (choisir la vue satellite et zoomer)   
        2. Que peut-on observer aux coordonnées  : 40° 41' 21.296'' N / 74° 2' 40.199'' O ?  
    <!-- === "Correction"
        1. La salle C3 C4 du Lycée Odilon Redon de Pauillac.
        2. La statue de la Liberté. -->

################################################################################################################################

Act2

<!-- ??? tip "corrections"
    1. [https://fr.wikipedia.org/wiki/Exchangeable_image_file_format](https://fr.wikipedia.org/wiki/Exchangeable_image_file_format){. target="_blank"}
    2. -
    3. Samsung GT-i9195 Galaxy S4 Mini
    4. Pas de localisation possible.
    5. -
    6. Localisation : Place Picard à Bordeaux. -->

<!-- 
??? tip "corrections"
    1. Le Pont de Pierre semble être au Groënland : les EXIF ont été modifiées.
    2. Oui, chaque téléphone permet de désactiver la localisation sur les photos.
    3. Non, les réseaux sociaux filtrent les EXIF.
    4. Par contre, on ne sait pas s'ils les filtrent en local ou sur leurs serveurs. -->

################################################################################################################################
Act3

### 2.2 Questions

#### 2.2.1. Cadastre

Recherche le numéro de la parcelle cadastrale de votre domicile. Mesure la superficie de cette parcelle. Tu peux t'aider du tutoriel vidéo ci-dessous.

<iframe width="696" height="390" src="https://www.youtube.com/embed/rbl2sF7zugk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

1. Territoires et transports / Foncier, cadastre et urbanisme / Parcelles cadastrales, puis mesure de surface (clé à molette)

#### 2.2.2. Gêne sonore

Monsieur X veut acheter une maison au 66 avenue du Maréchal Juin, à Biarritz. Mais il ne supporte pas le bruit. Que lui conseillez-vous ?

2. Territoires et transports / Foncier, cadastre et urbanisme / Plan d'exposition au bruit (PEB). Zone déconseillée pour le bruit.

#### 2.2.3. Drone de loisirs

Madame Y habite au domaine de Lisenne, à Tresses (33). Elle possède un drone de loisir et souhaite le faire voler à une altitude d’environ 60 mètres. En a-t-elle le droit ?

3. Territoires et transports / Foncier, cadastre et urbanisme / Restrictions UAS catégorie Ouverte et aéromodélisme. Hauteur max de vol : 50m


#### 2.2.4. Bureau de Poste

Monsieur Z habite 12 rue des églantiers à Cenon (33). Il veut savoir s'il peut aller à pied à un bureau de Poste en moins de 15 minutes. Pouvez-vous l'aider ?
(on pensera au clic-droit / *isochrone*)

4. Territoires et transports / Equipements publics / Services postaux. D'après l'isochrone 15mn à pied, un bureau de poste est accessible.

<!-- #### 2.2.5. Au XIXème siècle

Au XIXe siècle, comment appelait-on l’actuelle Avenue Thiers, sur la rive droite de Bordeaux ? -->

#### 2.3.1. Énigme 1

1. Ai-je le droit de prendre une photo aérienne de l’endroit aux coordonnées GPS : 44.829439 , -0.879641 ?
2. À proximité de cette zone, trouver une zone interdite à la prise de vue aérienne de forme triangulaire.
3. Que s'est-il passé à cet endroit durant la Seconde Guerre mondiale ?

<!-- ??? tip "Corrections"
    1. Territoires et transports / Description du territoire / Zones interdites à la prise de vue aérienne (ZIPVA). Zone interdite
    2. La zone est le camp de Souge.
    3. C'était un lieu où l'armée allemande fusillait les résistants. -->

#### 2.3.2. Énigme 2  

<figure markdown>
![image](data/fdcarte.png){width=60%}
<figcaption></figcaption>
</figure>

Sur cette image extraite de GéoPortail, le point jaune désigne une plage à laquelle s'intéresse **beaucoup** une très grande entreprise bien connue.  
Quelle est cette société ?  

??? abstract "indice"
    Développement Durable / Mer et Littoral

<!-- ??? tip "Correction"
    - Développement Durable / Mer et Littoral / Câbles et conduites sous-marines.
    - L'endroit est Saint-Hilaire-de-Riez en Vendée.
    - Google va y faire arriver un câble reliant la France et les USA : [https://france3-regions.francetvinfo.fr/pays-de-la-loire/vendee/vendee-arrivee-du-fur-cable-transatlantique-google-saint-hilaire-rietz-1799482.html](https://france3-regions.francetvinfo.fr/pays-de-la-loire/vendee/vendee-arrivee-du-fur-cable-transatlantique-google-saint-hilaire-rietz-1799482.html){. target="_blank"} -->

################################################################################################################################
Act 4

### 1.2. Seconde métrique

La route entre Labouheyre et Saugnac-et-Muret est une autoroute (vitesse maximale autorisée :
130 km/h), alors que toutes les autres routes sont des routes départementales (vitesse maximale
autorisée : 80 km/h).

1. Compléter le graphe ci-dessus en indiquant entre chaque ville le temps de parcours, si
l’automobiliste roule à la vitesse maximale autorisée.
2. Quel est le chemin le plus rapide ?

<!-- ??? tip "corrections"
	![image](data/graphePissos.png){width=50%}

	- Le chemin le plus court est : Lüe-Labouheyre-Commensacq-Pissos-Moustey (26 km)
	- Le chemin le plus rapide est : Lüe-Labouheyre-Liposthey-Pissos-Moustey (19 min) -->