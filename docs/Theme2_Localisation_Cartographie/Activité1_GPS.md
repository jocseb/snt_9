# Activité 1 : Coordonnées géographiques

## 1. Se repérer à la surface de la Terre

!!! example "Exercice 1 : coordonnées de villes du monde"
    === "Énoncé"
        Télécharger [ce document GeoGebra](data/geogebra_coord_calculatrice_3D_22022023.ggb){:target="_blank"} puis
        se rendre sur [le site GeoGebra](https://www.geogebra.org/calculator){:target="_blank"} et aller sur ![ouvrir](data/geogebra_ouvrir.png){width=10%} pour ouvrir le fichier téléchargé puis cliquer sur FICHIER LOCAL ![fichier local](data/geogebra_fichier_local.png){width=10%} et aller le chercher dans le dossier *Téléchargements*) pour obtenir ceci :  

        <figure markdown>
        ![](data/geogebra-export.png){:target="_blank"}
        <figcaption></figcaption>
        </figure>
        
        Sur l'animation GeoGebra apparaissent des villes (en noir) et un point bleu que l'on peut déplacer en maintenant appuyé le clic gauche de la souris.  
        Dans le tableau ci-dessous sont données des coordonnées de villes.  
        Associer à chaque ville le numéro qui lui correspond. 
     

        | numéro de la ville | Latitude | Longitude |
        |:---:|---|---|
        | 1 | 51° N | 0° O|
        | 2 | 49° N | 2° E|
        | 3 | 41° N | 116° O|
        | 4 | 0° N | 79° O|
        | 5 | 34° S | 18° E|
        | 6 | 34° S | 71° O|
        | 7 | 40° N | 74° E |

    === "Correction"

        | numéro de la ville | Latitude | Longitude | Ville |
        |:---:|---|---|---|
        | 1 | 51° N | 0° O| Londres |
        | 2 | 49° N | 2° E| Paris |
        | 3 | 41° N | 116° O| New-York |
        | 4 | 0° N | 79° O| Quito |
        | 5 | 34° S | 18° E| Le Cap |
        | 6 | 34° S | 71° O| Santiago|
        | 7 | 40° N | 74° E | Pékin |


<!-- Cliquer [ici](../Theme2_Localisation_Cartographie/cours.md/#11-convention-décriture-dangle){. target="_blank"} -->
Revenir au cours et aller à la partie ``convention d'écriture d'angle``.

<!-- ## 2. Les différentes conventions d'écriture d'angles

![](data/bordeaux.png){align=right}

Numériquement parlant, le format décimal (DD) des coordonnées géographiques est le plus pratique.

Sur le web (ou ailleurs), il arrive fréquemment que ces coordonnées ne soient pas données au format décimal, mais plutôt au format DMS (degrés, minutes, secondes)

Il faut donc les [convertir](https://fr.wikipedia.org/wiki/Syst%C3%A8me_sexag%C3%A9simal#Conversion_de_minutes_et_secondes_en_fraction_d%C3%A9cimale_de_degr%C3%A9){:target="_blank"}.

Par exemple, les coordonnées des villes sur Wikipedia sont données au format DMS (ci-contre celles de Bordeaux).

!!! info "Conversion"
    Comme pour la mesure du temps, une minute (d'arc) correspond à 1/60 de degré et une seconde (d'arc) à 1/60 de minute, soit 1/3600 de degré.

    La latitude de Bordeaux, en décimal, est donc obtenue par le calcul:
    
    44 + 50/60 + 16/3600 = 44,837777

    Enfin, cette latitude est positive car sa position par rapport à l'équateur est Nord. Une latitude 44°50'16'' S serait donc convertie -44,837777 en décimal.

!!! warning "Point ou virgule"
    Il ne faut pas confondre notre séparateur décimal, **la virgule**, avec celui des anglo-saxons, **le point**, qui est la norme sur tout système informatique. -->

## 2. Utilisation du site ```coordonnees-gps```

Il existe des sites en ligne qui proposent de manipuler très facilement des coordonnées GPS et de trouver les lieux correspondants, comme [https://www.coordonnees-gps.fr/](https://www.coordonnees-gps.fr/){:target="_blank"} par exemple.

!!! example "Exercice 2"
    === "Énoncé"
        En utilisant le site ```coordonnees-gps.fr```, répondre aux 2 questions suivantes :  
        1. Que trouve-t-on aux coordonnées : 45.193867 , -0.747963 ? (choisir la vue satellite et zoomer)
        2. Que peut-on observer aux coordonnées  : 40° 41' 21.296'' N / 74° 2' 40.199'' O ?  
   
    === "Correction"
        1. La salle C3 C4 du Lycée Odilon Redon de Pauillac.
        2. La statue de la Liberté à New-York.

Revenir au cours à la partie ``2. Comprendre la géolocalisation``.
